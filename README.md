Laravel 5 Modular
======================

Description
-----------
A laravel package to create a better house keeping of modules. It helps the developer to make a module in easiest way as possible. In one command, it will all the files you need for each module and it is presented as follow:

```shell
laravel/
    app/
    └── Modules/
        └── YourClass/
            ├── Controllers/
            │   └── YourClassController.php
            ├── Models/
            │   └── YourClass.php
            ├── Repositories/
            │   └── YourClassRepository.php
            └── Requests/
             	└── YourClassRequests.php

```

and of couse! you can change the path of each files.

Installation
------------

Execute the following command to get the latest version of the package:

```shell
composer require team-optimus/l5-modular
```

In your `config/app.php` add `TeamOptimus\L5Modular\Providers\L5ModularServiceProvider::class` to the end of the `providers` array:

```php
'providers' => [
    ...
    TeamOptimus\L5Modular\Providers\L5ModularServiceProvider::class,
],
```

Go to `App\Provider\RouteServiceProvider` and change the `$namespace` property to 

``php
protected $namespace = 'App';
``

Publish Configuration

```shell
php artisan vendor:publish --provider="TeamOptimus\L5Modular\Providers\L5ModularServiceProvider" 
```

Usage
-----

Creating a complete modular housekeeped files.

```shell
php artisan make:modular Post
```

Creating a single file.

```shell
php artisan make:mod-controller Post

php artisan make:mod-repository Post

php artisan make:mod-request Post
```

Appending route samples.

```shell
php artisan modular:route Post
```

Troubleshooting
---------------

None for now.


