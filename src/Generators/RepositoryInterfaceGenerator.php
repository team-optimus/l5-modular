<?php
namespace TeamOptimus\L5Modular\Generators;

use TeamOptimus\L5Modular\Generators\Parsers\SchemaParser;

/**
 * Class RepositoryInterfaceGenerator
 * @package TeamOptimus\L5Modular\Generators;
 */
class RepositoryInterfaceGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'interface';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'interface';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getInterfaceName() . '.php';
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('l5modular.base_path', app()->path() );
    }

    /**
     * Gets interface name based on model
     *
     * @return string
     */
    public function getInterfaceName()
    {

        return ucfirst( $this->getName() ) ."Repository";
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return  array_merge(parent::getReplacements(), [
            'interface_namespace' => $this->getRootNamespace(),
            'interface_name' => $this->getInterfaceName()
        ]);
    }


    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }

    public function getModel()
    {

        return  'use ' . str_replace([
            "\\",
            '/'
        ], '\\', $this->model) . ';';

    }

}
