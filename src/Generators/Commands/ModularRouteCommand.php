<?php
namespace TeamOptimus\L5Modular\Generators\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use TeamOptimus\L5Modular\Generators\Stub;
use TeamOptimus\L5Modular\Generators\RouteGenerator;
use File;

class ModularRouteCommand extends Command
{
    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'make:mod-route';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Populate route of the class.';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args() );
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        $route = new RouteGenerator([
            'name' => $this->argument('name'),
            'base' => $this->option('base') ? : $this->argument('name'),
        ]);

        $route->run();

        $this->info('Module routes has been populated successfully.');
    }

    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
        ];
    }

    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'base',
                null,
                InputOption::VALUE_OPTIONAL,
                'The base attributes.',
                null
            ],
        ];
    }

}
