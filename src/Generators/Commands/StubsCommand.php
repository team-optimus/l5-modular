<?php
namespace TeamOptimus\L5Modular\Generators\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use File;

class StubsCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'modular:stubs';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Populate stub on stubs override path.';

    /**
     * ControllerCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        if($path = config('l5modular.stubs_override_path')) {
            $base_path = base_path()."/".$path;
            File::copyDirectory( dirname(__DIR__) ."/Stubs" , $base_path );
            $this->info("Stubs populated successfully on '$base_path'.");   
        }else{
            $this->error('No assined override path.');   
        }
    }

    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
        ];
    }
}