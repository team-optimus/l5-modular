<?php
namespace TeamOptimus\L5Modular\Generators\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ModularCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'make:modular';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Create a new modular entity.';

    /**
     * @var Collection
     */
    protected $generators = null;

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {

        $name = $this->argument('name');
        $base = $this->option('base') ? : $name;
        if ($this->confirm('Would you like to populate routes? [y|N]')) {
            $this->call('make:mod-route', [
                'name'    => $name,
                '--base'    => $base,
            ]);
        }

        $this->call('modular:bind', [
            'name'    => $name,
            '--base'    => $base,
        ]);

        $this->call('make:mod-repository', [
            'name'        => $this->argument('name'),
            '--base'      => $base,
            '--fillable'  => $this->option('fillable'),
            '--rules'     => $this->option('rules'),
            '--force'     => $this->option('force')
        ]);

        $this->call('make:mod-controller', [
            'name'    => $this->argument('name'),
            '--base'      => $base,
            '--force'     => $this->option('force')
        ]);
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'base',
                null,
                InputOption::VALUE_OPTIONAL,
                'The base attributes.',
                null
            ],
            [
                'fillable',
                null,
                InputOption::VALUE_OPTIONAL,
                'The fillable attributes.',
                null
            ],
            [
                'rules',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'validator',
                null,
                InputOption::VALUE_OPTIONAL,
                'Adds validator reference to the repository.',
                null
            ],
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ]
        ];
    }
}