<?php
namespace TeamOptimus\L5Modular\Generators\Commands;

use Illuminate\Console\Command;
use TeamOptimus\L5Modular\Generators\RequestGenerator;
use TeamOptimus\L5Modular\Generators\FileAlreadyExistsException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RequestCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'make:mod-request';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Create a new modular request.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Request';


    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            (new RequestGenerator([
                'name' => $this->argument('name'),
                'base' => $this->option('base') ? : $this->argument('name'),
                'rules' => $this->option('rules'),
                'force' => $this->option('force'),
            ]))->run();
            $this->info("Request created successfully.");
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');
            return false;
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the request is being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'base',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'rules',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
        ];
    }
}
