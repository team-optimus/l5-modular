<?php
namespace TeamOptimus\L5Modular\Generators;

use TeamOptimus\L5Modular\Generators\Parsers\SchemaParser;

/**
 * Class RepositoryGenerator
 * @package TeamOptimus\L5Modular\Generators;
 */
class RepositoryGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'repository';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'repositories';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getRepositoryName() . '.php';
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('l5modular.base_path', app()->path() );
    }

    /**
     * Gets repository name based on model
     *
     * @return string
     */
    public function getRepositoryName()
    {

        return ucfirst( $this->getName() ) ."RepositoryEloquent";
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return  array_merge(parent::getReplacements(), [
            'repository_name' => $this->getRepositoryName(),
            'interface_name' => $this->getInterfaceName(),
            'interface_namespace' => $this->getInterfaceNamespace(),
            'model_name' => ucfirst( $this->getName() ),
            'model_namespace' => $this->getModelNameSpace(),
        ]);
    }

    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }

    public function getModelNameSpace()
    {

        return str_replace(["\\", '/'], '\\', $this->model) ;

    }


    /**
     * Gets repository full class name
     *
     * @return string
     */
    public function getInterfaceNamespace()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);
        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getInterfaceName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository);
    }


    /**
     * Gets repository full class name
     *
     * @return string
     */
    public function getInterfaceName()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);

        return $repositoryGenerator->getInterfaceName();
    }
}
