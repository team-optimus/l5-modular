<?php

namespace TeamOptimus\L5Modular\Generators;

use Illuminate\Support\Str;
/**
 * Class RouteGenerator
 * @package TeamOptimus\L5Modular\Generators;
 */
class RouteGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'route';

    /**
     * Modify declared route.
     *
     * @return void
     */
    public function run()
    {
        $route_path = $this->getPath();
        $route_file = \File::get( $route_path );
        \File::put( $route_path , $route_file .$this->getStub() );
 
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return config('l5modular.route');
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('l5modular.base_path', app()->path());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'route';
    }

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'namespace' => $this->getRouteNamespace(),
            'controller_name' => $this->getController(),
            'link_prefix' => $this->getLinkPrefix(),
            'route_prefix' => $this->getRoutePrefix(),
        ]);

    }

    /**
     * Gets singular name based on model
     *
     * @return string
     */
    public function getSingularName()
    {
        return Str::singular(lcfirst(ucwords($this->getClass())));
    }

    /**
     * Gets plural name based on model
     *
     * @return string
     */
    public function getPluralName()
    {

        return Str::plural (lcfirst(ucwords($this->getClass())));
    }

    public function getController()
    {
        $generator = new ControllerGenerator([
            'name' => $this->getClass(),
        ]);
        return $generator->getControllerName();
    }

    public function getRouteNamespace()
    {
        $path = str_replace("{base}", $this->getBase() , config('l5modular.paths.controllers') );
        return  str_replace( "/", "\\" ,  $path  );

    }

    public function getLinkPrefix()
    {
        return Str::kebab( $this->getSingularName() );
    }

    public function getRoutePrefix()
    {
        return Str::snake ( $this->getSingularName() );
    }

}
