<?php
namespace TeamOptimus\L5Modular\Generators;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package TeamOptimus\L5Modular\Generators;
 */
class FileAlreadyExistsException extends Exception
{
}