<?php

namespace TeamOptimus\L5Modular\Generators;

use Illuminate\Support\Str;
/**
 * Class ControllerGenerator
 * @package TeamOptimus\L5Modular\Generators;
 */
class ControllerGenerator extends Generator
{
    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'controller';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
    }

    public function getRoutableNamespace()
    {
        return parent::getConfigGeneratorClassPath( $this->getPathConfigNode() );
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'controllers';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath( $this->getPathConfigNode() , true) . '/' . $this->getControllerName() . '.php';
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('l5modular.base_path', app()->path() );
    }

    /**
     * Gets controller name based on model
     *
     * @return string
     */
    public function getControllerName()
    {

        return ucfirst( $this->getSingularName() ) ."Controller";
    }

    /**
     * Gets plural name based on model
     *
     * @return string
     */
    public function getPluralName()
    {

        return Str::plural (lcfirst(ucwords($this->getClass())));
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'controller_name' => $this->getControllerName(),
            // 'plural'     => $this->getPluralName(),
            // 'singular'   => $this->getSingularName(),
            'interface_namespace' => $this->getInterfaceNamespace(),
            'interface_name' => $this->getInterfaceName(),
            'appname'    => $this->getAppNamespace(),
            'request_namespace' => $this->getRequestNamespace(),
            'request_name' => $this->getRequestName(),
            'sc_singular' => Str::snake ( $this->getName() ),
            'uc_plural'     => ucfirst( $this->getPluralName() ) ,
            'uc_singular'     => ucfirst( $this->getName() ) 
        ]);
    }

    /**
     * Gets singular name based on model
     *
     * @return string
     */
    public function getSingularName()
    {
        return Str::singular(lcfirst(ucwords($this->getClass())));
    }


    public function getRequestNamespace()
    {
        $repositoryGenerator = new RequestGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);

        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository) . 'Request';

    }

    public function getRequestName()
    {
        $repositoryGenerator = new RequestGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);

        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return $repositoryGenerator->getRequestName();

    }


    /**
     * Gets repository full class name
     *
     * @return string
     */
    public function getInterfaceNamespace()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);
        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository) . 'Repository';
    }

    /**
     * Gets repository full class name
     *
     * @return string
     */
    public function getInterfaceName()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);

        return $repositoryGenerator->getInterfaceName();
    }

}
