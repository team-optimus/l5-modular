<?php
namespace TeamOptimus\L5Modular\Generators;

/**
 * Class BindingsGenerator
 * @package TeamOptimus\L5Modular\Generators
 */
class BindingsGenerator extends Generator
{

    /**
     * The placeholder for repository bindings
     *
     * @var string
     */
    public $bindPlaceholder = '//:end-bindings:';
    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'bindings';

    public function run()
    {

        // Add entity repository binding to the repository service provider
        $provider = \File::get($this->getPath());
        $repositoryInterface = '\\' . $this->getInterfaceNamespace() . "::class";
        $repositoryEloquent = '\\' . $this->geRepositoryNamespace() . "::class";
        \File::put(
            $this->getPath(), 
            str_replace($this->bindPlaceholder, "\$this->app->bind({$repositoryInterface}, $repositoryEloquent);" . PHP_EOL . '        ' . $this->bindPlaceholder, 
                $provider)
        );
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/Providers/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '.php';
    }

    /**
     * Get base path of destination file.
     *
     * @return string
     */
    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'provider';
    }

    /**
     * Gets repository full class name
     *
     * @return string
     */
    public function getInterfaceNamespace()
    {
        $repositoryGenerator = new RepositoryInterfaceGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);
        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository) . 'Repository';
    }
    /**
     * Gets eloquent repository full class name
     *
     * @return string
     */
    public function geRepositoryNamespace()
    {
        $repositoryGenerator = new RepositoryGenerator([
            'name' => $this->name,
            'base' => $this->getBase()
        ]);

        $repository = $repositoryGenerator->getRootNamespace() . '\\' . $repositoryGenerator->getName();

        return str_replace([
            "\\",
            '/'
        ], '\\', $repository) . 'RepositoryEloquent';
    }

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {

        return array_merge(parent::getReplacements(), [
            'repository' => $this->getRepository(),
            'eloquent' => $this->getEloquentRepository(),
            'placeholder' => $this->bindPlaceholder,
        ]);
    }
}
