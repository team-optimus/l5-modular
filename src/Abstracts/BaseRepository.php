<?php
namespace TeamOptimus\L5Modular\Abstracts;

use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository{

	protected $model;

	protected $app;

	public function __construct(Application $app)
    {
    	$this->app = $app;
        $this->makeModel();
    }

	/**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make( $this->model() );

        if (!$model instanceof Model) {
        	die("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function find($id)
	{
		$resource = $this->model->find($id);
        
		if(!$resource) abort(404);

		return $resource;
	}

	public function all()
	{
		return $this->model->all();
	}

    public function create($attributes)
    {
        $attributes = $this->cleanData( $attributes );
        return $this->model->create($attributes);
    }

    public function update($attributes, $id)
    {
        $attributes = $this->cleanData( $attributes );
        $data = $this->model->find($id)->fill($attributes);
        $data->save();
         return $data;
    }

    public function delete($id)
    {
        $resource = $this->model->find($id);
        $resource->delete();
    }

    protected function cleanData( $attributes )
    {
        unset($attributes['_method']);
        unset($attributes['_token']);
        return $attributes;
    }

    public function query()
    {
        return $this->model->query();
    }

    public function with($values)
    {
        return $this->model->with($values);
    } 

}