<?php

namespace TeamOptimus\L5Modular\Providers;

use Illuminate\Support\ServiceProvider;

class L5ModularServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../resources/l5modular.php' => config_path('l5modular.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\BindCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\ModularCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\ControllerCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\RequestCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\RepositoryCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\ModularRouteCommand');
        $this->commands('TeamOptimus\L5Modular\Generators\Commands\StubsCommand');
    }
}
